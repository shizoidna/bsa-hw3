module.exports = {
  env: {
    browser: true,
    es2020: true,
    node: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 11,
    sourceType: 'module',
  },
  rules: {
    'no-console': 'off',
    'no-unused-vars': 'off',
    'no-undef': 'off',
    'no-alert': 'off',
    'import/prefer-default-export': 'off',
    'no-use-before-define': 'off',
    'no-param-reassign': 'off',
    'default-case': 'off',
    'no-useless-return': 'off',
  },
};
