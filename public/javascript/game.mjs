const username = sessionStorage.getItem('username');

if (!username) {
  window.location.replace('/login');
}

const state = {
  selectedRoom: null,
  interval: null,
};

document.addEventListener('click', (event) => {
  if (event.target.hasAttribute('data-add-room-btn')) {
    addRoom();
    return;
  }

  if (event.target.hasAttribute('data-join-room-btn')) {
    const roomName = event.target.dataset.joinRoomBtn;
    joinRoom(roomName);
    return;
  }

  if (event.target.hasAttribute('data-leave-room-btn')) {
    const roomName = event.target.dataset.leaveRoomBtn;
    leaveRoom(roomName);
    return;
  }

  if (event.target.hasAttribute('data-ready-btn')) {
    socket.emit('message', { code: 'set-ready', data: { isReady: true } });
    return;
  }

  if (event.target.hasAttribute('data-not-ready-btn')) {
    socket.emit('message', { code: 'set-ready', data: { isReady: false } });
    return;
  }
});

const socket = io('', {
  transports: ['websocket'],
  query: { username },
});

socket.on('message', (message) => {
  switch (message.code) {
    case 'rooms':
      renderRoomCards(message.data.rooms);
      if (state.selectedRoom !== null) {
        state.selectedRoom = message
          .data.rooms.find((room) => room.name === state.selectedRoom.name);

        renderGamePage();

        if (state.selectedRoom.users.every((user) => user.isReady) && state.interval === null) {
          startTimer();
        } else if (state.selectedRoom.users
          .some((user) => !user.isReady) && state.interval !== null) {
          clearInterval(state.interval);
          state.interval = null;
          state.timeToStart = state.selectedRoom.timeToStart;
          state.timeToGame = state.selectedRoom.timeToGame;
        }
      }
      break;
    case 'join-room-success':
      state.selectedRoom = message.data.room;
      state.timeToStart = message.data.room.timeToStart;
      state.timeToGame = message.data.room.timeToGame;
      clearInterval(state.interval);
      state.interval = null;
      renderGamePage();
      break;
    case 'leave-room-success':
      renderRoomsPage();
      break;
  }
});

socket.on('err', (error) => {
  switch (error.code) {
    case 'user-exist':
      alert(error.message);
      disconnectAndRedirect(socket);
      break;
    default:
      alert(error.message);
  }
});

function disconnectAndRedirect(sock) {
  sock.disconnect();
  sessionStorage.removeItem('username');
  window.location.replace('/login');
}

function addRoom() {
  const roomName = prompt('Input room name');

  if (roomName) {
    socket.emit('message', { code: 'create-room', data: { roomName } });
  }
}

function joinRoom(roomName) {
  if (roomName) {
    socket.emit('message', { code: 'join-room', data: { roomName } });
  }
}

function leaveRoom(roomName) {
  if (roomName) {
    socket.emit('message', { code: 'leave-room', data: { roomName } });
  }
}

function renderRoomCards(rooms) {
  const showRooms = rooms
    .filter((room) => room.users.length < room.maxUsers)
    .filter((room) => room.users.some(({ isReady }) => !isReady)); // timer not started

  const roomsContainer = document.querySelector('.rooms');
  let html = '';

  showRooms.forEach((room) => {
    html += `
      <div class="room">
        <span class="connected-users">${room.users.length} connected users</span>
        <h3 class="room-name">${room.name}</h3>
        <button data-join-room-btn="${room.name}" class="join-room-btn btn">Join</button>
      </div>
  `;
  });

  roomsContainer.innerHTML = html;
}

function renderGamePage() {
  const room = state.selectedRoom;

  if (!room) {
    return;
  }

  const gamePage = document.getElementById('game-page');
  const roomsPage = document.getElementById('rooms-page');

  roomsPage.classList.add('display-none');
  gamePage.classList.remove('display-none');

  let usersHtml = '';
  let btnHtml = '';

  room.users.forEach((user) => {
    let userName;

    if (user.name === username) {
      userName = `${user.name} (you)`;
      btnHtml = user.isReady === true
        ? '<button data-not-ready-btn class="room-ready btn-lg">Not Ready</button>'
        : '<button data-ready-btn class="room-ready btn-lg">Ready</button>';
    } else {
      userName = `${user.name}`;
    }

    usersHtml += `
        <div class="user">
          <div class="room-info-group">
            <div class="readiness ${user.isReady ? 'ready' : 'not-ready'}"></div>
            <span class="username">${userName}</span>
          </div>
          <div class="meter">
            <span class="meter-progress"></span>
          </div>
        </div>
      `;
  });

  gamePage.innerHTML = `
    <div class="room-page pad">
      <div class="room-info">
        <div class="rooms-options">
          <h2>${room.name}</h2>
          <button data-leave-room-btn="${room.name}" class="to-rooms-btn btn"><span class="back-icon"> < </span>Back to rooms</button>
        </div>
        <div class="room-users">
          ${usersHtml}
        </div>
      </div>

      <div class="room-game">
        <div class="game-readiness ${state.interval === null ? '' : 'display-none'}">
          <div class="game-readiness-container">
            <div class="btn-group">
               ${btnHtml}
            </div>
          </div>
        </div>
        <div class="game-timer ${state.interval !== null && state.timeToStart >= 0 ? '' : 'display-none'}">
          <div class="game-timer-container">
            <span class="timer-count">${state.timeToStart}</span>
          </div>
        </div>
        <div class="game-process ${state.interval !== null && state.timeToStart < 0 ? '' : 'display-none'}">
          <div class="game-process-container">
            <span class="process-timer">
            ${state.timeToGame} seconds left
            </span>
          </div>
          <div class="game-text-container">
            <p class="game-text-paragraph">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis egestas tellus non lobortis.
              Morbi eget odio dui. Maecenas feugiat, urna quis rhoncus efficitur, lacus arcu ornare justo, suscipit
              aliquet est est vitae eros. Donec ut commodo velit, et maximus leo. Suspendisse potenti. Mauris justo
              mauris, elementum sit amet sapien sed, tincidunt auctor dui. Vivamus congue vitae dui eu blandit.
            </p>
          </div>
        </div>
      </div>
    </div>
  `;
}

function renderRoomsPage() {
  const gamePage = document.getElementById('game-page');
  const roomsPage = document.getElementById('rooms-page');

  gamePage.classList.add('display-none');
  roomsPage.classList.remove('display-none');
  state.selectedRoom = null;
}

function startTimer() {
  state.interval = setInterval(() => {
    state.timeToStart -= 1;
    renderGamePage();

    if (state.timeToStart < 0) {
      clearInterval(state.interval);
      state.interval = null;
      startRace();
    }
  }, 1000);
}

function startRace() {
  state.interval = setInterval(() => {
    state.timeToGame -= 1;
    renderGamePage();

    if (state.timeToGame < 0) {
      clearInterval(state.interval);
      state.interval = null;
      // THE END
    }
  }, 1000);
}
