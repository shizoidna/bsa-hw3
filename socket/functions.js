import { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME } from './config';

export function getAllConnectedUsernames(io) {
  return (Object.keys(io.sockets.sockets)
    .filter(Boolean)
    .map((sockId) => io.sockets.sockets[sockId].nickname));
}

export function getAllRoomsInfo(io) {
  const connectedUserIds = Object.keys(io.sockets.sockets);

  return Object.keys(io.sockets.adapter.rooms)
    .filter((roomName) => !connectedUserIds.includes(roomName))
    .map((roomName) => getRoomInfo(io, roomName));
}

export function getRoomInfo(io, roomName) {
  return {
    name: roomName,
    users: getRoomUsers(io, roomName),
    maxUsers: MAXIMUM_USERS_FOR_ONE_ROOM,
    timeToStart: SECONDS_TIMER_BEFORE_START_GAME,
    timeToGame: SECONDS_FOR_GAME,
  };
}

export function getRoomUsers(io, roomName) {
  return Object.keys(io.sockets.adapter.rooms[roomName].sockets).map((userId) => ({
    name: io.sockets.sockets[userId].nickname,
    isReady: io.sockets.sockets[userId].isReady,
  }));
}

export function leaveAllRooms(socket) {
  Object.keys(socket.rooms)
    .filter((room) => room !== socket.id)
    .forEach((room) => socket.leave(room));
}

export function joinRoom(socket, io, roomName) {
  socket.join(roomName, () => {
    socket.emit('message', { code: 'join-room-success', data: { room: getRoomInfo(io, roomName) } });
    emitRooms(socket, io, true);
  });
}

export function createRoomHandler(socket, io, roomName) {
  const roomNames = getAllRoomsInfo(io).map(({ name }) => name);

  if (roomNames.includes(roomName)) {
    socket.emit('err', { code: 'room-exist', message: 'Room with such name already exist.' });
  } else {
    leaveAllRooms(socket);
    joinRoom(socket, io, roomName);
  }
}

export function joinRoomHandler(socket, io, roomName) {
  const rooms = getAllRoomsInfo(io);

  const selectedRoom = rooms.find((room) => room.name === roomName);

  if (!selectedRoom) {
    socket.emit('err', { code: 'room-does-not-exist', message: 'Cant join the room, because it does not exist.' });
  } else if (selectedRoom.users.length >= MAXIMUM_USERS_FOR_ONE_ROOM) {
    socket.emit('err', { code: 'room-is-full', message: 'Cant join the room, because it is full.' });
  } else {
    leaveAllRooms(socket);
    joinRoom(socket, io, roomName);
  }
}

export function leaveRoomHandler(socket, io, roomName) {
  socket.isReady = false;
  socket.leave(roomName);
  socket.emit('message', { code: 'leave-room-success', data: {} });
  emitRooms(socket, io, true);
}

export function emitRooms(socket, io, isBroadcast) {
  if (isBroadcast) {
    io.emit('message', { code: 'rooms', data: { rooms: getAllRoomsInfo(io) } });
  } else {
    socket.emit('message', { code: 'rooms', data: { rooms: getAllRoomsInfo(io) } });
  }
}
