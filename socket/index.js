import {
  createRoomHandler,
  emitRooms,
  getAllConnectedUsernames,
  joinRoomHandler,
  leaveRoomHandler,
} from './functions';

export default (io) => {
  io.on('connection', (socket) => {
    const { username } = socket.handshake.query;

    if (getAllConnectedUsernames(io).includes(username)) {
      socket.emit('err', {
        code: 'user-exist',
        message: 'User with such name is already exist.',
      });
      socket.disconnect(true);
      return;
    }

    socket.nickname = username;
    socket.isReady = false;
    emitRooms(socket, io, false);

    socket.on('disconnect', () => {
      emitRooms(socket, io, true);
    });

    socket.on('message', (message) => {
      switch (message.code) {
        case 'create-room':
          createRoomHandler(socket, io, message.data.roomName);
          break;
        case 'join-room':
          joinRoomHandler(socket, io, message.data.roomName);
          break;
        case 'leave-room':
          leaveRoomHandler(socket, io, message.data.roomName);
          break;
        case 'set-ready':
          socket.isReady = message.data.isReady;
          emitRooms(socket, io, true);
          break;
      }
    });
  });
};
